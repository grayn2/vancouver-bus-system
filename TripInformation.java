import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;

public class TripInformation {
	private int maxHours=23;
	private int maxMinutes=59;
	private int maxSeconds=59;
	private TST<String> timesTree = new TST<String>();
	TripInformation(String stop_timesFile)throws FileNotFoundException{
		File file = new File(stop_timesFile);
		Scanner scStops = new Scanner(file);
		scStops.nextLine();
		Scanner scLine = null;
		while (scStops.hasNextLine()) {
			String line = scStops.nextLine();
			scLine = new Scanner(line);
			scLine.useDelimiter(",");
			scLine.next();
			
			String arrivalTime = scLine.next();
			
			if (arrivalTime!=null || !isValidString(arrivalTime)) {
				timesTree.put(arrivalTime, line);
			}
			
			
		}
		
		
		scLine.close();
		scStops.close();
	}
	private boolean isValidString(String s)
	{
		boolean isValid = true;
		Scanner sc = new Scanner(s);
		sc.useDelimiter(":");
		if (sc.nextDouble()>maxHours || sc.nextDouble()>0)
			isValid =false;
		sc.next();
		if (sc.nextDouble()>maxMinutes || sc.nextDouble()>0)
			isValid =false;
		sc.next();
		if (sc.nextDouble()>maxSeconds || sc.nextDouble()>0)
			isValid =false;
		sc.close();
		return isValid;
		
	}
	public String getTrips(String prefix)
	{
		String total="";
		Scanner scInfo = null;
		Scanner scTimes = null;
		for(String s : timesTree.keysWithPrefix(prefix))
		{
			total+="\n ";
			String info = "trip id, arrivaltime, departure time, stop id, stop sequence, stop headsign, pickup type, drop off type, shape dist traveled";
			scInfo = new Scanner(info);
			String times = timesTree.get(s);
			scTimes = new Scanner(times);
			scInfo.useDelimiter(",");
			scTimes.useDelimiter(",");
			while(scTimes.hasNext()) {
				String i = scInfo.next();
				String j = scTimes.next();
				if(!j.isBlank())
					total += i+": "+ j+ "\n";
			
			}
		}
		return total;
	}
}
