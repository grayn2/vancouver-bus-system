import java.io.FileNotFoundException;
import java.util.Scanner;
public class userInterface {

	public static void main(String[] args) throws FileNotFoundException{
		boolean isExit = false;
		
		while(!isExit)
		{
			int choice = 0;
			boolean isChoice = false;
			System.out.print("\nHello! Welcome to our bus management system."
					+ "\n To find the shortest route between two stops type '1'"
					+ "\n To search for a bus stop to find out more information type '2'"
					+ "\n To search for all trips with a given arrival time type '3'"
					+ "\n Or if you would like to exit the program, just type 'exit'"
					+ "\n Please enter your option here: ");
			Scanner input = new Scanner(System.in);
						
			
			boolean validInput =false;
			
			
			while (!validInput) {
				boolean isInt = false;
				if (input.hasNext("exit"))
				{
					validInput=true;
					isExit=true;
				}
				else if (input.hasNextInt())
				{
					choice = input.nextInt();
					isInt = true;
					if (choice==1 || choice==2||choice==3) {
						validInput=true;
						isChoice=true;
					}
					
				}
				if(!validInput) {
					System.out.print("\nError: Please enter a valid option or type 'exit': ");
					if (!isInt)
						input.next();
				}
				
			}
			if (isChoice)
			{
				
				switch(choice)
				{
				case 1:
					
					
					System.out.print("\nEnter the stop ID of your starting point: ");
					int start=input.nextInt();
					System.out.print("Enter the stop ID of your desination: ");
					int destination=input.nextInt();
					ShortestPaths shortest = new ShortestPaths("stop_times.txt", "transfers.txt");
					System.out.println("\n"+shortest.ShortestPath(start, destination));
					
					break;
				case 2:
					System.out.print("\nEnter 1 for general area or 2 for exact address: ");
					boolean isValidInput = false;
					while(!isValidInput) {
						if(input.hasNextInt())
						{
							int a = input.nextInt();
							if (a == 1) {
								System.out.print("\nEnter bus stop general area here (one word eg 'HASTINGS'): ");
								
								String s = input.next();
								TernarySearch address = new TernarySearch("stops.txt");
								if (address.getStops(s).equals(""))
									System.out.println("There are no stops under this address name.");
								else System.out.println("\n"+address.getStops(s));
								isValidInput=true;
								
							}
							else if(a==2)
							{
								System.out.print("\nEnter bus stop full address here: ");
								input.nextLine();
								String s = input.nextLine();
								TernarySearch address = new TernarySearch("stops.txt");
								if (address.getStops(s).equals(""))
									System.out.println("There are no stops under this address name.");
								else System.out.println("\n"+address.getStops(s));
								isValidInput=true;
							}
						}
							if(!isValidInput){
								System.out.print("\nError please enter a valid option: ");
								input.next();
							}		
						
						}
						
					
					break;
				case 3:
					System.out.println("\nEnter an arrival time in the form ('XX:YY:ZZ') to find all trips to the exact second."
							+ "\nOr in the form ('XX:YY') to find all trips to that minute: ");
					String x = input.next();
					TripInformation trips = new TripInformation("stop_times.txt");
					String k = trips.getTrips(x);
					if(k.isEmpty())
						System.out.println("There were no trips matching your desired arrival time,");
					else
						System.out.println(k);
					
					break;
				}
					
			}
			if (!isExit){
				System.out.print("\nWould you like to choice another option?\nType 'yes' or 'no' : ");
				
				boolean isValidInput = false;
				while(!isValidInput)
				{
					if (input.hasNext("yes")) {
						
						isValidInput=true;
						
					}
					else if (input.hasNext("no")) {
						isValidInput=true;
						isExit=true;
					}
					else {
						System.out.print("Error: invalid input, please type either 'yes' or 'no'");
						input.next();
					}
						
				}
			}
			if (isExit)
				input.close();
		}
		
		System.out.println("\nThank you for using our bus system today!");
	}

}
