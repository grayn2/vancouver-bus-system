import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;
public class ShortestPaths {
	private final static double maxDouble = Double.MAX_VALUE;
	private final static int maxMatrix = 12479;
	private double adjacencyMatrix[][] = new double[maxMatrix][maxMatrix];
	
	
	private String stopTimesFile;
	private String transferesFile;
	
	private int lastTripId;
	private int tripId;
	private int from_stop_id; 
	private int to_stop_id; 
	private int transfer_type;
	private int cost;
	private double time;
	
	ShortestPaths(String stopTimesFile, String transferesFile) throws FileNotFoundException{
		
		this.stopTimesFile = stopTimesFile;
		this.transferesFile=transferesFile;
		constructMatrix();
		
	}
	
	
	private void constructMatrix() throws FileNotFoundException{
		String line;
		Scanner scLine;
		lastTripId = 0;
		tripId = 0;
		from_stop_id = 0; 
		to_stop_id = 0;
		//initialize all adjacency matrix values to maxDouble value
		for (int i=0;i<maxMatrix;i++)
		{
			for(int j=0; j<maxMatrix;j++)
			{
				if(i==j)
					adjacencyMatrix[i][j]=0;
				else
					adjacencyMatrix[i][j]=maxDouble;
			}
		}
		
		File transfereTimes = new File(transferesFile);
		Scanner scTransferes = new Scanner(transfereTimes);
		scTransferes.nextLine();
		//finding cost of transferes
		//by reading through all transferes in transfers.txt
		while(scTransferes.hasNextLine())
		{
			line = scTransferes.nextLine();
			scLine = new Scanner(line);
			scLine.useDelimiter(",");
			
			
			from_stop_id = scLine.nextInt();
			to_stop_id = scLine.nextInt();
			transfer_type = scLine.nextInt();
			
			if (transfer_type==2)
			{
				time = scLine.nextDouble();
				adjacencyMatrix[from_stop_id][to_stop_id] = time/100;
			}
			
			else adjacencyMatrix[from_stop_id][to_stop_id] = 2;
			
			scLine.close();
			
		}
		scTransferes.close();
		
		File stopTimes = new File(stopTimesFile);

		Scanner scStopTimes = new Scanner(stopTimes);
		

		scStopTimes.nextLine();

		cost=1;
		
		while(scStopTimes.hasNextLine()) 
		{

			line = scStopTimes.nextLine();
			scLine = new Scanner(line);
			scLine.useDelimiter(",");

			lastTripId = tripId;
			tripId = scLine.nextInt();
			
			scLine.next();
			scLine.next();
			
			from_stop_id=to_stop_id;
			to_stop_id=scLine.nextInt();
			
			if(lastTripId == tripId)
				adjacencyMatrix[from_stop_id][to_stop_id]=cost;
			scLine.close();
			
			
		}
		
		scStopTimes.close();
		return;
		
	}
	
public String ShortestPath(int start, int finish) throws FileNotFoundException{
		
		if (start==finish) {
			return "cost: �0"+
				"\nRoute: " +start+" -> "+finish;
		}	
		
		
		double distTo[]= new double[maxMatrix];
		int edgeTo[] = new int[maxMatrix];
		boolean isVisited[] = new boolean[maxMatrix];
		
		isVisited[start] = true;
		
		int position = start;
		double shortestPath = maxDouble;
		
		for (int i=0;i<maxMatrix;i++)
		{
			distTo[i] = maxDouble;
		}
		distTo[start] = 0;
		//dijkstra shortest path
		for (int i = 0; i<maxMatrix;i++)
		{
			
			for (int k=0; k<maxMatrix; k++)
			{
				if(isVisited[k]==false && adjacencyMatrix[position][k]<maxDouble)
					relaxEdge(position, k, distTo, edgeTo);
			}
			isVisited[position] = true;
			
			shortestPath=maxDouble;
			for(int j=0; j<maxMatrix; j++)
			{
				if (isVisited[j]==false && distTo[j]<shortestPath)
				{
					shortestPath = distTo[j];
					position=j;
					
				}
			}
			
		}
		if (distTo[finish]==maxDouble)
			return "this route does not exist";
		return formatString(start, finish, distTo, edgeTo);
		
	}
	
	private String formatString(int start, int finish, double[] distTo, int[] edgeTo)
	{
		
		String route = "";
		
		int tracker = finish;
		
		while(start!=tracker)
		{
			if(start!=edgeTo[tracker])
				route= " -> " + edgeTo[tracker] + route;
			tracker = edgeTo[tracker]; //tracker going to previous node
		}
		route= start + route;
		
		return "cost: �"+ distTo[finish]+ 
				"\nRoute: "+ route + " -> " +finish;
	}
	
	//from lecture slides
	 private void relaxEdge(int from, int to, double[] distTo, int[] edgeTo) {
	    	if(distTo[to] > distTo[from] + adjacencyMatrix[from][to]) {
	    		distTo[to] = distTo[from] + adjacencyMatrix[from][to];
	    		edgeTo[to] = from;
	    	}
	    }
	
	
}
