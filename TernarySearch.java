
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class TernarySearch {
	
	private TST<String> addressTree = new TST<String>();
	TernarySearch(String stopsFileName) throws FileNotFoundException
	{
		
		File file = new File(stopsFileName);
		Scanner scStops = new Scanner(file);
		scStops.nextLine();
		Scanner scLine = null;
		while (scStops.hasNextLine()) {
			String line = scStops.nextLine();
			scLine = new Scanner(line);
			scLine.useDelimiter(",");
			scLine.next();
			scLine.next();
			
			String stopName = scLine.next();
			String fixedName = addressFixer(stopName);
			
			if (fixedName!=null) {
				addressTree.put(fixedName, line);
			}
			
		}
		
		scLine.close();
		scStops.close();
	}
	public String getStops(String prefix)
	{
		
		String total = "";
		if (prefix.length()>15)
		{
			for(String s : addressTree.keysThatMatch(prefix))
			{
				total+="\n ";
				String info = "stop id, stop code, stop name, stop description , stop latitude, stop longitude, zone id, stop url, location type, parent station";
				
				Scanner sc = new Scanner(info);
				sc.useDelimiter(",");
				
				String lineInfo = addressTree.get(s);
				Scanner scLine = new Scanner(lineInfo);
				scLine.useDelimiter(",");
				while(scLine.hasNext()) {
					String i = scLine.next();
					String j = sc.next();
					if(!i.equals(" "))
						total+=j+": "+ i+"\n";
				}
				sc.close();
				scLine.close();
				
				
			}
		}
		else {
			for(String s : addressTree.keysWithPrefix(prefix))
			{
				total+="\n ";
				String info = "stop id, stop code, stop name, stop description , stop latitude, stop longitude, zone id, stop url, location type, parent station";
				
				Scanner sc = new Scanner(info);
				sc.useDelimiter(",");
				
				String lineInfo = addressTree.get(s);
				Scanner scLine = new Scanner(lineInfo);
				scLine.useDelimiter(",");
				while(scLine.hasNext()) {
					String i = scLine.next();
					String j = sc.next();
					if(!i.equals(" "))
						total+=j+": "+ i+"\n";
				}
				sc.close();
				scLine.close();
				
				
			}
		}
		
		return total;
	}
	

	private String addressFixer(String address) {
		if (address!=null) {
			String keyWord = address.substring(0,2);
			
			if (keyWord.equals("WB") || keyWord.equals("NB") || keyWord.equals("SB")|| keyWord.equals("EB")) {
				address = address.substring(3);
				address = address.concat(" " + keyWord);
			}
		}
		return address;
			
		
	}
	
	
	
}
